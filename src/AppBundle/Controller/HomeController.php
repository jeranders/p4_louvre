<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 18/05/2017
 * Heure: 09:15
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Commande;
use AppBundle\Form\Type\CommandeTicketType;
use AppBundle\Form\Type\CommandeType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class HomeController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $deleteSession = $this->get('session')->remove('commande');

        $form = $this->get('app.booking')->commande($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            return $this->redirectToRoute('recap');
        }

        return $this->render('default/index.html.twig', array(
            'form' => $form->createView(),
            'deleteSession' => $deleteSession
        ));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/recap", name="recap")
     */
    public function recapAction()
    {
        $recap = $this->get('session')->get('commande');
        if (null === $recap) {
            return $this->redirectToRoute('homepage');
            throw new NotFoundHttpException("La réservation n'existe pas.");
        }
        return $this->render('default/recap.html.twig', array(
            'recap' => $recap
        ));
    }

    /**
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/valide_commande", name="valide_commande", methods="POST")
     */
    public function valideCommandeAction(Request $request)
    {
        $stripe = $this->get('app.booking')->validation($request);

        $sessionCommande = $this->get('session')->get('commande');

        if (null === $sessionCommande) {
            return $this->redirectToRoute('homepage');
            throw new NotFoundHttpException("La réservation n'existe pas.");
        }

        return $this->render('default/valide_commande.html.twig', array(
            'commande' => $sessionCommande,
            'stripe' => $stripe
        ));
    }

    /**
     * @Route("/validation", name="validation")
     */
    public function validationAction()
    {
        return $this->render('default/validation.html.twig');
    }

}