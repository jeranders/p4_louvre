<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 19/05/2017
 * Heure: 16:24
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Commande;
use AppBundle\Validators\CountTicket;
use AppBundle\Validators\DayClose;
use AppBundle\Validators\DayPast;
use AppBundle\Validators\HalfDay;
use AppBundle\Validators\Holiday;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

/**
 * Class CommandeType
 * @package AppBundle\Form\Type
 */
class CommandeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', TextType::class, array(
                'label' => 'Votre prénom: ',
                'constraints' => array(
                    new NotBlank(),
                    new Length(
                        array(
                            'min' => 3,
                            'max' => 120,
                            'minMessage' => 'Le prénom doit contenir au moins 3 caractères !',
                            'maxMessage' => 'Le prénom ne doit pas contenir plus de 120 caractères !',
                        )
                    )
                )
            ))
            ->add('lastName', TextType::class, array(
                'label' => 'Votre nom :',
                'constraints' => array(
                    new NotBlank(),
                    new Length(
                        array(
                            'min' => 3,
                            'max' => 100,
                            'minMessage' => 'Le nom doit contenir au moins 3 caractères !',
                            'maxMessage' => 'Le nom ne doit pas contenir plus de 100 caractères !',
                        )
                    )
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Adresse email :',
                'constraints' => array(
                    new NotBlank()
                )
            ))
            ->add('dateVisit', DateType::class, array(
                'label' => 'Date de la visite',
                'constraints' => array(
                    new NotBlank(),
                    new DayClose(),
                    new CountTicket(),
                    new DayPast(),
                    new Holiday(),
//                    new HalfDay()
                ),
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'html5' => false,
                'required' => true
            ))
            ->add('dayType', ChoiceType::class, array(
                'label' => 'Type de billet :',
                'choices' => array(
                    'Journée complète' => true,
                    'Demi-journée' => false,
                )
            ))
            ->add('tickets', CollectionType::class, array(
                'entry_type' => TicketType::class,
                'entry_options' => array(
                    'label' => 'Ticket :',
                ),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,
                'label' => false,
                'attr' => array(
                    'class' => 'collection'
                )
            ))
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Commande::class,
        ));
    }

}