<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 08:01
 */

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Class CountTicket
 * @package AppBundle\Validators
 * @Annotation
 */
class CountTicket extends Constraint
{
    public $message = 'Désolé il n\'y a plus de billet disponible pour cette date';

    public function validateBy()
    {
        return get_class($this).'Validator';
    }
}