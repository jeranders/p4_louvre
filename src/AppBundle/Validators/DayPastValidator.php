<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 07:30
 */

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DayPastValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if(!$value){
            return;
        }

        $dayVisit = $value->getTimestamp();

        $today = mktime(0, 0, 0, date('m'), date('d'), date('Y'));

        if (intval($dayVisit) < intval($today))
        {
            $this->context->addViolation($constraint->message);
        }
    }
}