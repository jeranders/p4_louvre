<?php
/**
 * Created by PhpStorm.
 * User: Jeranders
 * Date: 29/05/2017
 * Time: 08:52
 */

namespace AppBundle\Validators;

use Symfony\Component\Validator\Constraint;

/**
 * Class HalfDay
 * @package AppBundle\Validators
 */
class HalfDay extends Constraint
{
    public $message = 'Vous ne pouvez pas commander un billet "Journée" après 14h';

    /**
     * @return string
     */
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
}