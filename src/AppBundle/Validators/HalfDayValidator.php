<?php
/**
 * Created by PhpStorm.
 * User: Jeranders
 * Date: 29/05/2017
 * Time: 08:09
 */

namespace AppBundle\Validators;

use AppBundle\Entity\Commande;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
/**
 * Class HalfDayValidator
 * @package AppBundle\Validators
 */
class HalfDayValidator extends ConstraintValidator
{
    /**
     * @param mixed      $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if(!$value){
            return;
        }

        if ($value->getDayType() == 1)
        {
            $actual = new \DateTime();

            $hours = date('H', $value->getTimeStamp());

            if ($hours > $actual && $hours < 10) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        }



    }
}