<?php
/**
 * User: Jérôme Brechoire
 * Email : brechoire.j@gmail.com
 * Date: 30/05/2017
 * Heure: 07:24
 */

namespace AppBundle\Validators;


use Symfony\Component\Validator\Constraint;

/**
 * Class Holiday
 * @package AppBundle\Validators
 * @Annotation
 */
class Holiday extends Constraint
{
    public $message = 'Le musée est fermé à cette date';

    public function validateBy()
    {
        return get_class($this).'Validator';
    }
}